import os
import bs4 as bs4
import requests
import random
import vk_api
import json
import links

COMMANDS = ["ПРИВЕТ", 
            "ПОГОДА", 
            "ВРЕМЯ", 
            "ПОКА",
            "МУР",
            "МЯУ",                            
            "*ПОГЛАДИТЬ",
            "*ПОИГРАТЬ",
            "*ПОКОРМИТЬ",
            "ПОМОЩЬ",
            "ЦИТАТА",
            "*КОМИКС"]

MEOW_AND_PURR = ["Мурр!",
                "Мяу!",
                "Мяумяу!",
                "Мурмяу!"]

def _files_to_array(directory):
    array = []
    array = os.listdir(directory)
    for i in range(len(array)):
        array[i] = directory + array[i]
    return array

# Получение случайного значения из массива
def _get_random_array_value(curArray):
    lenArray = len(curArray)
    randomPos = random.randint(0, lenArray - 1)
    return curArray[randomPos]

def _get_meow_or_purr():
    result = _get_random_array_value(MEOW_AND_PURR)
    return result          

def new_message(message, USERNAME):
    
    messageArr = message.upper().split(" ")

    # Привет
    if COMMANDS[0] in messageArr[0]:
        return f"Мяу! Привет, {USERNAME}!" + '\n' + 'Если хочешь узнать мои команды, набери "помощь"'

    # Погода
    elif messageArr[0] == COMMANDS[1]:
        if len(messageArr) > 1:
            if messageArr[1] != '':
                return _get_weather(messageArr[1])                
            else:    
                return _get_weather()
        else:    
            return _get_weather()        

    # Время
    elif messageArr[0] == COMMANDS[2]:
        return _get_time()

    # Пока
    elif messageArr[0] == COMMANDS[3]:
        return f"Мяй... Пока, {USERNAME}..."

    # Если в строке есть "мур" или "мяу"
    elif (COMMANDS[4] in messageArr[0]) or (COMMANDS[5] in messageArr[0]):
        return _get_meow_or_purr()

    # *Погладить
    elif messageArr[0] == COMMANDS[6]:            
        return _get_meow_or_purr()

    # *Поиграть
    elif messageArr[0] == COMMANDS[7]:
        return _get_meow_or_purr()

    # *Покормить
    elif messageArr[0] == COMMANDS[8]:
        return _get_meow_or_purr() 

    # Помощь               
    elif messageArr[0] == COMMANDS[9]:
        return "Мяу! Вот фразы, которые я понимаю: " + '\n' + '\n' + "ПРИВЕТ" + '\n' + "ПОГОДА" + '\n' + "ВРЕМЯ" + '\n' + "ПОКА" + '\n' + "МУР" + '\n' + "МЯУ" + '\n' + "*ПОГЛАДИТЬ" + '\n' + "*ПОИГРАТЬ" + '\n' + "*ПОКОРМИТЬ" + '\n' + "ПОМОЩЬ"  

    # Цитата
    elif messageArr[0] == COMMANDS[10]:                    
        return _get_random_array_value(links._CITATION) 

    # *Комикс
    elif messageArr[0] == COMMANDS[11]:                    
        return _get_meow_or_purr()       
    
    else:
        return "Я ни зняю такого..."

# Получение времени:
def _get_time():
    result = ''
    result = 'Мяу! Я смотрю время на https://my-calend.ru/date-and-time-today' + '\n'
    request = requests.get("https://my-calend.ru/date-and-time-today")
    b = bs4.BeautifulSoup(request.text, "html.parser")
    result = result + _clean_all_tag_from_str(str(b.select(".page")[0].findAll("h2")[1])).split()[1] + '(Москва)'
    return result 

# Метод для очистки от ненужных тэгов
#@staticmethod
def _clean_all_tag_from_str(string_line):

    """
    Очистка строки stringLine от тэгов и их содержимых
    :param string_line: Очищаемая строка
    :return: очищенная строка
    """

    result = ""
    not_skip = True
    for i in list(string_line):
        if not_skip:
            if i == "<":
                not_skip = False
            else:
                result += i
        else:
            if i == ">":
                not_skip = True

    return result

# Получение погоды
#@staticmethod
def _get_weather(city: str = "пермь") -> list:
    
    city = city.lower()
    result = ''
    result = result + ('Мяу! Я смотрю погоду на sinoptik.com.ru') + '\n'
    result = result + ('Город: ' + city) + '\n'
    
    try:        
        request = requests.get("https://sinoptik.com.ru/погода-" + city)
        b = bs4.BeautifulSoup(request.text, "html.parser")

        tempClass = b.select(' .weather__article_main_temp')
        temptext = tempClass[0].getText()

        weatherClass = b.select(' .weather__article_description-text')        
        weather = weatherClass[0].getText()        


        result = result + ('Температура сейчас:' + temptext) + '\n'
        result = result + ('Погода :' + '\n' + weather) + '\n'

    except Exception as e:            
        result = result + f'Ошибка: {str(e)}'
    return result               