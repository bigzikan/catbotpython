import telebot
import threading
import datetime
import requests
import bs4 as bs4
import time
import RequestsBot
import links
#from telebot import apihelper

tokenfile = open('tokenTelegram.txt', "r", encoding="UTF-8")
token = tokenfile.read()
#apihelper.proxy = {'https':'185.242.117.160:8080'}
tgBot = telebot.TeleBot(token)

_COMMANDS = RequestsBot.COMMANDS
_MEOW_AND_PURR = RequestsBot.MEOW_AND_PURR

_PET_THE_CAT = RequestsBot._files_to_array("./pet/")
_PLAY_WITH_THE_CAT = RequestsBot._files_to_array("./play/")
_FEED_THE_CAT = RequestsBot._files_to_array("./feed/")
_CAT_DONT_KNOW = RequestsBot._files_to_array("./dontknow/")
_CITATION = links._CITATION
_COMICS = RequestsBot._files_to_array("./comics/")

def _get_attachment(curArray, message, typeattachment = 0):        

    attach_name = RequestsBot._get_random_array_value(curArray)
    cur_attach = open(attach_name, 'rb')

    if typeattachment == 0: 
        #загрузка gif
        tgBot.send_document(message.chat.id, data=cur_attach, caption=RequestsBot.new_message(message.text, message.from_user.first_name))   

    elif typeattachment == 1:
        #загрузка фото
        tgBot.send_photo(message.chat.id, photo=cur_attach, caption=RequestsBot.new_message(message.text, message.from_user.first_name))        

    cur_attach.close()    


def new_attachment(message):       

    messageArr = message.html_text.upper().split(" ")

    # Погладить
    if messageArr[0] == _COMMANDS[6]:
        return _get_attachment(_PET_THE_CAT, message)

    # Поиграть
    elif messageArr[0] == _COMMANDS[7]:
        return _get_attachment(_PLAY_WITH_THE_CAT, message)

    # Покормить
    elif messageArr[0] == _COMMANDS[8]:
        return _get_attachment(_FEED_THE_CAT, message)

    # Комикс
    elif messageArr[0] == _COMMANDS[11]:
        return _get_attachment(_COMICS, message, 1)            

    else:
        return _get_attachment(_CAT_DONT_KNOW, message)    

@tgBot.message_handler(content_types=['text'])
def send_text(message):
    if message.text[0] == "*":
        new_attachment(message)
    else:        
        tgBot.send_message(message.chat.id, RequestsBot.new_message(message.text, message.from_user.first_name))

def start():
    try:
        tgBot.polling()

    except Exception as e:
        with open("Tglogs.txt", "a") as myfile:
            myfile.write(f'{time.ctime()} {e}\n')   

