import bs4 as bs4
import requests
import random
import vk_api
import json
from vk_api import VkUpload
import links
import os
from vk_api.longpoll import VkLongPoll, VkEventType
import time
from commander.commander import Commander
import vk_api
import RequestsBot

class VkBot:

    def __init__(self, user_id):
        #print("\nСоздан объект бота!")

        self.session = requests.Session()

        self._USER_ID = user_id
        self._USERNAME = self._get_user_name_from_vk_id(user_id)

        self._COMMANDS = RequestsBot.COMMANDS
        self._MEOW_AND_PURR = RequestsBot.MEOW_AND_PURR

        self._PET_THE_CAT = RequestsBot._files_to_array("./pet/")
        self._PLAY_WITH_THE_CAT = RequestsBot._files_to_array("./play/")
        self._FEED_THE_CAT = RequestsBot._files_to_array("./feed/")
        self._CAT_DONT_KNOW = RequestsBot._files_to_array("./dontknow/")
        self._CITATION = links._CITATION
        self._COMICS = RequestsBot._files_to_array("./comics/")


    def _get_user_name_from_vk_id(self, user_id):
        request = requests.get("https://vk.com/id" + str(user_id))
        bs = bs4.BeautifulSoup(request.text, "html.parser")

        user_name = RequestsBot._clean_all_tag_from_str(bs.findAll("title")[0])

        return user_name.split()[0]  
    
    def _get_attachment(self, vk_session, curArray, userID, typeattachment = 0):        
        attachments = []

        if typeattachment == 0: 
            #загрузка gif. gif прикрепляется как документ
            gif_name = RequestsBot._get_random_array_value(curArray)
            jsonk = vk_session.document_message(doc=gif_name, title='Документ', peer_id=userID)['doc']
            attachments.append('doc{}_{}'.format(jsonk['owner_id'], jsonk['id']))

        elif typeattachment == 1:
            #загрузка фото. Можно отправлять, как документ, но будет открываться в отдельной вкладке
            photo_name = RequestsBot._get_random_array_value(curArray)
            photo = vk_session.photo_messages(photo_name)[0]
            attachments.append('photo{}_{}'.format(photo['owner_id'], photo['id']))


        return attachments

    def new_attachment(self, message, vkSession):       

        messageArr = message.upper().split(" ")
        upload = VkUpload(vkSession)

        # Погладить
        if messageArr[0] == self._COMMANDS[6]:
            return self._get_attachment(upload, self._PET_THE_CAT, self._USER_ID)

        # Поиграть
        elif messageArr[0] == self._COMMANDS[7]:
            return self._get_attachment(upload, self._PLAY_WITH_THE_CAT, self._USER_ID)

        # Покормить
        elif messageArr[0] == self._COMMANDS[8]:
            return self._get_attachment(upload, self._FEED_THE_CAT, self._USER_ID)

        # Комикс
        elif messageArr[0] == self._COMMANDS[11]:
            return self._get_attachment(upload, self._COMICS, self._USER_ID, 1)            

        else:
            return self._get_attachment(upload, self._CAT_DONT_KNOW, self._USER_ID)  

def start():
    try:  
        # API-ключ созданный ранее для vk
        tokenfile = open('token.txt', "r", encoding="UTF-8")
        token = tokenfile.read()
        # Авторизуемся как сообщество
        vk = vk_api.VkApi(token=token)
        # Работа с сообщениями
        longpoll = VkLongPoll(vk)

        commander = Commander()
        #print("Server started")
        for event in longpoll.listen():
            
            try:
                if event.type == VkEventType.MESSAGE_NEW:

                    if event.to_me:

                        #print(f'New message from {event.user_id}', end='')           

                        bot = VkBot(event.user_id)

                        if event.text[0] == "/":
                            vk.method('messages.send', {'user_id': event.user_id, 'message': commander.do(event.text[1::]), 'random_id': random.randint(0, 2048)})
                        elif event.text[0] == "*":                
                            vk.method('messages.send', {'user_id': event.user_id, 'message': RequestsBot.new_message(event.text, bot._USERNAME), 'random_id': random.randint(0, 2048), 'attachment': bot.new_attachment(event.text, vk)})    
                        else:
                            vk.method('messages.send', {'user_id': event.user_id, 'message': RequestsBot.new_message(event.text, bot._USERNAME), 'random_id': random.randint(0, 2048)})
                        #print('Text: ', event.text)
                        #print()
            except Exception as e:
                with open("Vklogs.txt", "a") as myfile:
                    myfile.write(f'{time.ctime()} {e}\n')

    except Exception as e:
        with open("Vklogs.txt", "a") as myfile:
            myfile.write(f'{time.ctime()} {e}\n')              