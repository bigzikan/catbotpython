#! /usr/bin/env python
# -*- coding: utf-8 -*-
import tg_bot
import vk_bot
import threading
import time

try:
    vkBotThread = threading.Thread(target=vk_bot.start, args=())
    #t.daemon = True
    vkBotThread.start()

    tgBotThread = threading.Thread(target=tg_bot.start, args=())
    #t.daemon = True
    tgBotThread.start()
except Exception as e:
    with open("logs.txt", "a") as myfile:
        myfile.write(f'{time.ctime()} {e}\n')