import os
from cx_Freeze import setup, Executable
import sys

os.environ['TCL_LIBRARY'] = 'c:/python36/tcl/tcl8.6'
os.environ['TK_LIBRARY'] = 'c:/python36/tcl/tk8.6' 
buildOptions = dict( packages = [], excludes = [], include_files=['c:/python36/DLLs/tcl86t.dll', 'c:/python36/DLLs/tk86t.dll'] )

base = None
if (sys.platform == "win32"):
    base = "Win32GUI"

setup(
    name = "VkCatBot",
    version = "1.1",
    description = "",
    executables = [Executable("Main.py")]
)